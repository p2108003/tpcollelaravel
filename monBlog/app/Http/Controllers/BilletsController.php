<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorebilletsRequest;
use App\Http\Requests\UpdatebilletsRequest;
use App\Models\Billet;

class BilletsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billets = Billet::all();
        return view('billets',compact('billets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorebilletsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorebilletsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Billet  $billet
     * @return \Illuminate\Http\Response
     */
    public function show(Billet $billet)
    {
        $commentaires = $billet->commentaire;
        return view('commentaires',compact('billet','commentaires'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Billet  $billets
     * @return \Illuminate\Http\Response
     */
    public function edit(Billet $billets)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatebilletsRequest  $request
     * @param  \App\Models\Billet  $billets
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatebilletsRequest $request, Billet $billet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Billet  $billets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Billet $billet)
    {
        //
    }
}
