<?php

namespace App\Models;

use App\Models\Commentaire;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Billet extends Model
{
    use HasFactory;

    public function Commentaire() {
        return $this->hasMany(Commentaire::class);
    }
}
