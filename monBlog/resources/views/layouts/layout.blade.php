<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/monBlog.css"/>
        <title>@yield('titre')</title>
    </head>
    <body>
        <div id="global">
            <header id="titreBlog"><h1>@yield('titrePage')</h1></header>
            <div id="contenu">@yield('contenu')</div>
            <footer id="piedBlog">Monblog - copyright 3A Info - 2022</footer>
        </div>
    </body>
</html>