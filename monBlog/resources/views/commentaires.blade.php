@extends('layouts.layout')

@section('titre')
    Commentaires du billet
@endsection

@section('titrePage')
    Commentaires
@endsection

@section('contenu')
    <h1 id="titreBillet">{{ $billet->BIL_Titre }}</h1>
    <p>{{ $billet->BIL_Date }}</p>
    <p>{{ $billet->BIL_Contenu }}</p>
    <hr>
    <p id="titreReponses">Réponses à {{ $billet->BIL_Titre }}</p>
    @foreach($commentaires as $com)
    <p>{{ $com->COM_Auteur }} dit :<br>
    {{ $com->COM_Contenu }}<br></p>
    @endforeach
@endsection