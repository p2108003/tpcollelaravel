@extends('layouts.layout')

@section('titre')
    Tous les billets du blog
@endsection

@section('titrePage')
    Tous les billets :
@endsection

@section('contenu')
    @foreach($billets as $billet)
    <h1 id="titreBillet"><a href="{{ route('billets.show', $billet->id)}}">{{ $billet->BIL_Titre }}</a></h1>
    <p>{{ $billet->BIL_Date }}<br>
    {{ $billet->BIL_Contenu }}<br></p>
    @endforeach
@endsection